package com.example.cook;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.cook.data.Banner;
import com.example.cook.data.Category;
import com.example.cook.data.Recipe;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class ApiService {
    private Context context;

    public ApiService(Context context) {
        this.context = context;
    }

    public void getRecipes(Response.Listener<List<Recipe>> listener) {
        GsonRequest<List<Recipe>> recipeRequest = new GsonRequest<>(
                Request.Method.GET,
                "https://api.myjson.com/bins/n7bxs",
                new TypeToken<List<Recipe>>() {
                }.getType(),
                listener,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Tag", "onErrorResponse: ");
                    }
                }
        );

        Volley.newRequestQueue(context).add(recipeRequest);
    }

    public void getBanners(Response.Listener<List<Banner>> listener) {
        GsonRequest<List<Banner>> bannerRequest = new GsonRequest<>(
                Request.Method.GET,
                "https://api.myjson.com/bins/110sw0",
                new TypeToken<List<Banner>>() {
                }.getType(),
                listener,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Tag", "onErrorResponse: ");
                    }
                }
        );

        Volley.newRequestQueue(context).add(bannerRequest);
    }

    public void getCategories(Response.Listener<List<Category>> listener) {
        GsonRequest<List<Category>> categoryRequest = new GsonRequest<>(
                Request.Method.GET,
                "https://api.myjson.com/bins/v0bog",
                new TypeToken<List<Category>>() {
                }.getType(),
                listener,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Tag", "onErrorResponse: ");
                    }
                }
        );

        Volley.newRequestQueue(context).add(categoryRequest);
    }

}
